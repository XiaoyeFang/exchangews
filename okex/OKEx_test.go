package okex

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
	"time"
)

//
var config2 = &exchangeWs.APIConfig{
	Endpoint: "https://www.okex.com",
	//HttpClient: &http.Client{
	//	Transport: &http.Transport{
	//		Proxy: func(req *http.Request) (*url.URL, error) {
	//			return &url.URL{
	//				Scheme: "socks5",
	//				Host:   "127.0.0.1:1080"}, nil
	//		},
	//	},
	//}, //需要代理的这样配置
	HttpClient:    http.DefaultClient,
	ApiKey:        "",
	ApiSecretKey:  "",
	ApiPassphrase: "",
}

var okex = NewOKEx(config2) //线上请用APIBuilder构建

func TestOKExSpot_GetAccount(t *testing.T) {
	t.Log(okex.GetAccount())
}

func TestOKExSpot_BatchPlaceOrders(t *testing.T) {
	t.Log(okex.OKExSpot.BatchPlaceOrders([]exchangeWs.Order{
		exchangeWs.Order{
			Cid:       okex.UUID(),
			Currency:  exchangeWs.XRP_USD,
			Amount:    10,
			Price:     0.32,
			Side:      exchangeWs.BUY,
			Type:      "limit",
			OrderType: exchangeWs.ORDER_TYPE_ORDINARY,
		},
		{
			Cid:       okex.UUID(),
			Currency:  exchangeWs.EOS_USD,
			Amount:    1,
			Price:     5.2,
			Side:      exchangeWs.BUY,
			OrderType: exchangeWs.ORDER_TYPE_ORDINARY,
		},
		exchangeWs.Order{
			Cid:       okex.UUID(),
			Currency:  exchangeWs.XRP_USD,
			Amount:    10,
			Price:     0.33,
			Side:      exchangeWs.BUY,
			Type:      "limit",
			OrderType: exchangeWs.ORDER_TYPE_ORDINARY,
		}}))
}

func TestOKExSpot_LimitBuy(t *testing.T) {
	t.Log(okex.OKExSpot.LimitBuy("0.001", "9910", exchangeWs.BTC_USD))
}

func TestOKExSpot_CancelOrder(t *testing.T) {
	t.Log(okex.OKExSpot.CancelOrder("2a647e51435647708b1c840802bf70e5", exchangeWs.BTC_USD))

}

func TestOKExSpot_GetOneOrder(t *testing.T) {
	t.Log(okex.OKExSpot.GetOneOrder("42152275c599444aa8ec1d33bd8003fb", exchangeWs.BTC_USD))
}

func TestOKExSpot_GetUnfinishOrders(t *testing.T) {
	t.Log(okex.OKExSpot.GetUnfinishOrders(exchangeWs.EOS_BTC))
}

func TestOKExSpot_GetTicker(t *testing.T) {
	t.Log(okex.OKExSpot.GetTicker(exchangeWs.BTC_USD))
}

func TestOKExSpot_GetDepth(t *testing.T) {
	dep, err := okex.OKExSpot.GetDepth(2, exchangeWs.EOS_BTC)
	assert.Nil(t, err)
	t.Log(dep.AskList)
	t.Log(dep.BidList)
}

func TestOKExFuture_GetFutureTicker(t *testing.T) {
	t.Log(okex.OKExFuture.GetFutureTicker(exchangeWs.BTC_USD, "BTC-USD-190927"))
	t.Log(okex.OKExFuture.GetFutureTicker(exchangeWs.BTC_USD, exchangeWs.QUARTER_CONTRACT))
	t.Log(okex.OKExFuture.GetFutureDepth(exchangeWs.BTC_USD, exchangeWs.QUARTER_CONTRACT, 2))
	t.Log(okex.OKExFuture.GetContractValue(exchangeWs.XRP_USD))
	t.Log(okex.OKExFuture.GetFutureIndex(exchangeWs.EOS_USD))
	t.Log(okex.OKExFuture.GetFutureEstimatedPrice(exchangeWs.EOS_USD))
}

func TestOKExFuture_GetFutureUserinfo(t *testing.T) {
	t.Log(okex.OKExFuture.GetFutureUserinfo())
}

func TestOKExFuture_GetFuturePosition(t *testing.T) {
	t.Log(okex.OKExFuture.GetFuturePosition(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT))
}

func TestOKExFuture_PlaceFutureOrder(t *testing.T) {
	t.Log(okex.OKExFuture.PlaceFutureOrder(exchangeWs.EOS_USD, exchangeWs.THIS_WEEK_CONTRACT, "5.8", "1", exchangeWs.OPEN_BUY, 0, 10))
}

func TestOKExFuture_PlaceFutureOrder2(t *testing.T) {
	t.Log(okex.OKExFuture.PlaceFutureOrder2(0, &exchangeWs.FutureOrder{
		Currency:     exchangeWs.EOS_USD,
		ContractName: exchangeWs.QUARTER_CONTRACT,
		OType:        exchangeWs.OPEN_BUY,
		OrderType:    exchangeWs.ORDER_TYPE_ORDINARY,
		Price:        5.9,
		Amount:       10,
		LeverRate:    10}))
}

func TestOKExFuture_FutureCancelOrder(t *testing.T) {
	t.Log(okex.OKExFuture.FutureCancelOrder(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT, "e88bd3361de94512b8acaf9aa154f95a"))
}

func TestOKExFuture_GetFutureOrder(t *testing.T) {
	t.Log(okex.OKExFuture.GetFutureOrder("3145664744431616", exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT))
}

func TestOKExFuture_GetUnfinishFutureOrders(t *testing.T) {
	t.Log(okex.OKExFuture.GetUnfinishFutureOrders(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT))
}

func TestOKExFuture_MarketCloseAllPosition(t *testing.T) {
	t.Log(okex.OKExFuture.MarketCloseAllPosition(exchangeWs.BTC_USD, exchangeWs.THIS_WEEK_CONTRACT, exchangeWs.CLOSE_BUY))
}

func TestOKExFuture_GetRate(t *testing.T) {
	t.Log(okex.OKExFuture.GetRate())
}

func TestOKExFuture_GetKlineRecords(t *testing.T) {
	since := time.Now().Add(-24 * time.Hour).Unix()
	kline, err := okex.OKExFuture.GetKlineRecords(exchangeWs.QUARTER_CONTRACT, exchangeWs.BTC_USD, exchangeWs.KLINE_PERIOD_4H, 0, int(since))
	assert.Nil(t, err)
	for _, k := range kline {
		t.Logf("%+v", k.Kline)
	}
}

func TestOKExWallet_GetAccount(t *testing.T) {
	t.Log(okex.OKExWallet.GetAccount())
}

func TestOKExWallet_Transfer(t *testing.T) {
	t.Log(okex.OKExWallet.Transfer(TransferParameter{
		Currency:     exchangeWs.EOS.Symbol,
		From:         SPOT,
		To:           SPOT_MARGIN,
		Amount:       20,
		InstrumentId: exchangeWs.EOS_USDT.ToLower().ToSymbol("-")}))
}

func TestOKExWallet_Withdrawal(t *testing.T) {
	t.Log(okex.OKExWallet.Withdrawal(WithdrawParameter{
		Currency:    exchangeWs.EOS.Symbol,
		Amount:      100,
		Destination: 2,
		ToAddress:   "",
		TradePwd:    "",
		Fee:         "0.01",
	}))
}

func TestOKExWallet_GetDepositAddress(t *testing.T) {
	t.Log(okex.OKExWallet.GetDepositAddress(exchangeWs.BTC))
}

func TestOKExWallet_GetWithDrawalFee(t *testing.T) {
	t.Log(okex.OKExWallet.GetWithDrawalFee(nil))
}

func TestOKExWallet_GetDepositHistory(t *testing.T) {
	t.Log(okex.OKExWallet.GetDepositHistory(&exchangeWs.BTC))
}

func TestOKExWallet_GetWithDrawalHistory(t *testing.T) {
	t.Log(okex.OKExWallet.GetWithDrawalHistory(&exchangeWs.XRP))
}

func TestOKExMargin_GetMarginAccount(t *testing.T) {
	t.Log(okex.OKExMargin.GetMarginAccount(exchangeWs.EOS_USDT))
}

func TestOKExMargin_Borrow(t *testing.T) {
	t.Log(okex.OKExMargin.Borrow(exchangeWs.BorrowParameter{
		Currency:     exchangeWs.EOS,
		CurrencyPair: exchangeWs.EOS_USDT,
		Amount:       10,
	}))
}

func TestOKExMargin_Repayment(t *testing.T) {
	t.Log(okex.OKExMargin.Repayment(exchangeWs.RepaymentParameter{
		BorrowParameter: exchangeWs.BorrowParameter{
			Currency:     exchangeWs.EOS,
			CurrencyPair: exchangeWs.EOS_USDT,
			Amount:       10},
		BorrowId: "123"}))
}

func TestOKExMargin_PlaceOrder(t *testing.T) {
	t.Log(okex.OKExMargin.PlaceOrder(&exchangeWs.Order{
		Currency:  exchangeWs.EOS_USDT,
		Amount:    0.2,
		Price:     6,
		Type:      "limit",
		OrderType: exchangeWs.ORDER_TYPE_ORDINARY,
		Side:      exchangeWs.SELL,
	}))
}

func TestOKExMargin_GetUnfinishOrders(t *testing.T) {
	t.Log(okex.OKExMargin.GetUnfinishOrders(exchangeWs.EOS_USDT))
}

func TestOKExMargin_CancelOrder(t *testing.T) {
	t.Log(okex.OKExMargin.CancelOrder("3174778420532224", exchangeWs.EOS_USDT))
}

func TestOKExMargin_GetOneOrder(t *testing.T) {
	t.Log(okex.OKExMargin.GetOneOrder("3174778420532224", exchangeWs.EOS_USDT))
}
