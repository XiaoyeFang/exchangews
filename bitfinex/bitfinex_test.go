package bitfinex

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var bfx = New(http.DefaultClient, "", "")

func TestBitfinex_GetTicker(t *testing.T) {
	ticker, _ := bfx.GetTicker(exchangeWs.ETH_BTC)
	t.Log(ticker)
}

func TestBitfinex_GetDepth(t *testing.T) {
	dep, _ := bfx.GetDepth(2, exchangeWs.ETH_BTC)
	t.Log(dep.AskList)
	t.Log(dep.BidList)
}
