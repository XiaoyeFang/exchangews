package fcoin

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"log"
	"net"
	"net/http"
	"net/url"
	"testing"
	"time"
)

var fcws = NewFCoinWs(&http.Client{
	Transport: &http.Transport{
		Proxy: func(req *http.Request) (*url.URL, error) {
			return url.Parse("socks5://127.0.0.1:1080")
			return nil, nil
		},
		Dial: (&net.Dialer{
			Timeout: 10 * time.Second,
		}).Dial,
	},
	Timeout: 10 * time.Second,
})

func init() {
	fcws.ProxyUrl("socks5://127.0.0.1:1080")
	fcws.SetCallbacks(printfTicker, printfDepth, printfTrade, nil)
}

func printfTicker(ticker *exchangeWs.Ticker) {
	log.Println("ticker:", ticker)
}
func printfDepth(depth *exchangeWs.Depth) {
	log.Println("depth:", depth)
}
func printfTrade(trade *exchangeWs.Trade) {
	log.Println("trade:", trade)
}
func printfKline(kline *exchangeWs.Kline, period int) {
	log.Println("kline:", kline)
}

func TestFCoinWs_GetTickerWithWs(t *testing.T) {
	return
	fcws.SubscribeTicker(exchangeWs.BTC_USDT)
	time.Sleep(time.Second * 10)
}
func TestFCoinWs_GetDepthWithWs(t *testing.T) {
	return
	fcws.SubscribeDepth(exchangeWs.BTC_USDT, 20)
	time.Sleep(time.Second * 10)
}
func TestFCoinWs_GetKLineWithWs(t *testing.T) {
	return
	fcws.SubscribeKline(exchangeWs.BTC_USDT, exchangeWs.KLINE_PERIOD_1MIN)
	time.Sleep(time.Second * 120)
}
func TestFCoinWs_GetTradesWithWs(t *testing.T) {
	return
	fcws.SubscribeTrade(exchangeWs.BTC_USDT)
	time.Sleep(time.Second * 10)
}
func TestNewFCoinWs(t *testing.T) {
	fcws.SubscribeTrade(exchangeWs.BTC_USDT)
	//fcws.SubscribeKline(exchangeWs.BTC_USDT, exchangeWs.KLINE_PERIOD_1MIN)
	//fcws.SubscribeDepth(exchangeWs.BTC_USDT, 20)
	fcws.SubscribeTicker(exchangeWs.BTC_USDT)
	time.Sleep(time.Minute * 10)

}
