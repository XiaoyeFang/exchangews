package fcoin

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"testing"
)

var fm = &FCoinMargin{ft}

func TestFCoinMargin_Borrow(t *testing.T) {
	return
	t.Log(fm.Borrow(exchangeWs.BorrowParameter{
		CurrencyPair: exchangeWs.BTC_USDT,
		Currency:     exchangeWs.USDT,
		Amount:       100,
	}))
}

func TestFCoinMargin_Repayment(t *testing.T) {
	return
	t.Log(fm.Repayment(exchangeWs.RepaymentParameter{
		BorrowParameter: exchangeWs.BorrowParameter{
			CurrencyPair: exchangeWs.BTC_USDT,
			Currency:     exchangeWs.USDT,
			Amount:       100.065,
		},
		BorrowId: "uQ7Gzird8kW0rbsC9Cu-RlcY7cGgrog23dEVugBh9JA",
	}))
}

func TestFCoinMargin_AssetTransferIn(t *testing.T) {
	return
	t.Log(fm.AssetTransferIn(exchangeWs.USDT, "80", ASSETS, exchangeWs.BTC_USDT))
}

func TestFCoinMargin_GetMarginAccount(t *testing.T) {
	//return
	t.Log(fm.GetMarginAccount(exchangeWs.EOS_USDT))
}

func TestFCoinMargin_GetAccount(t *testing.T) {
	t.Log(fm.GetAccount())
}
func TestFCoinMargin_GetOrderHistorys2(t *testing.T) {
	t.Log(fm.GetOrderHistorys2(exchangeWs.EOS_USDT, 1, 100,
		"submitted,partial_filled,partial_canceled,filled,canceled"))
}
