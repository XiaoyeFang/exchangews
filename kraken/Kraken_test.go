package kraken

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var k = New(http.DefaultClient, "", "")
var BCH_XBT = exchangeWs.NewCurrencyPair(exchangeWs.BCH, exchangeWs.XBT)

func TestKraken_GetDepth(t *testing.T) {
	dep, err := k.GetDepth(2, exchangeWs.BTC_USD)
	assert.Nil(t, err)
	t.Log(dep)
}

func TestKraken_GetTicker(t *testing.T) {
	ticker, err := k.GetTicker(exchangeWs.ETC_BTC)
	assert.Nil(t, err)
	t.Log(ticker)
}

func TestKraken_GetAccount(t *testing.T) {
	acc, err := k.GetAccount()
	assert.Nil(t, err)
	t.Log(acc)
}

func TestKraken_LimitSell(t *testing.T) {
	ord, err := k.LimitSell("0.01", "6900", exchangeWs.BTC_USD)
	assert.Nil(t, err)
	t.Log(ord)
}

func TestKraken_LimitBuy(t *testing.T) {
	ord, err := k.LimitBuy("0.01", "6100", exchangeWs.NewCurrencyPair(exchangeWs.XBT, exchangeWs.USD))
	assert.Nil(t, err)
	t.Log(ord)
}

func TestKraken_GetUnfinishOrders(t *testing.T) {
	ords, err := k.GetUnfinishOrders(exchangeWs.NewCurrencyPair(exchangeWs.XBT, exchangeWs.USD))
	assert.Nil(t, err)
	t.Log(ords)
}

func TestKraken_CancelOrder(t *testing.T) {
	r, err := k.CancelOrder("O6EAJC-YAC3C-XDEEXQ", exchangeWs.NewCurrencyPair(exchangeWs.XBT, exchangeWs.USD))
	assert.Nil(t, err)
	t.Log(r)
}

func TestKraken_GetTradeBalance(t *testing.T) {
	//	k.GetTradeBalance()
}

func TestKraken_GetOneOrder(t *testing.T) {
	ord, err := k.GetOneOrder("ODCRMQ-RDEID-CY334C", exchangeWs.BTC_USD)
	assert.Nil(t, err)
	t.Log(ord)
}
