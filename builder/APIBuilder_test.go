package builder

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/XiaoyeFang/exchangeWs"
	"testing"
)

var builder = NewAPIBuilder()

func TestAPIBuilder_Build(t *testing.T) {
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.OKCOIN_COM).GetExchangeName(), exchangeWs.OKCOIN_COM)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.HUOBI_PRO).GetExchangeName(), exchangeWs.HUOBI_PRO)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.ZB).GetExchangeName(), exchangeWs.ZB)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.BIGONE).GetExchangeName(), exchangeWs.BIGONE)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.OKEX).GetExchangeName(), exchangeWs.OKEX)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.POLONIEX).GetExchangeName(), exchangeWs.POLONIEX)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.KRAKEN).GetExchangeName(), exchangeWs.KRAKEN)
	assert.Equal(t, builder.APIKey("").APISecretkey("").Build(exchangeWs.FCOIN_MARGIN).GetExchangeName(), exchangeWs.FCOIN_MARGIN)
	assert.Equal(t, builder.APIKey("").APISecretkey("").BuildFuture(exchangeWs.HBDM).GetExchangeName(), exchangeWs.HBDM)
}
