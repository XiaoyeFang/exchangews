package huobi

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"log"
	"testing"
	"time"
)

func TestNewHbdmWs(t *testing.T) {
	ws := NewHbdmWs()
	ws.ProxyUrl("socks5://127.0.0.1:1080")

	ws.SetCallbacks(func(ticker *exchangeWs.FutureTicker) {
		log.Println(ticker.Ticker)
	}, func(depth *exchangeWs.Depth) {
		log.Println(">>>>>>>>>>>>>>>")
		log.Println(depth.ContractType, depth.Pair)
		log.Println(depth.BidList)
		log.Println(depth.AskList)
		log.Println("<<<<<<<<<<<<<<")
	}, func(trade *exchangeWs.Trade, s string) {
		log.Println(s, trade)
	})

	t.Log(ws.SubscribeTicker(exchangeWs.BTC_USD, exchangeWs.QUARTER_CONTRACT))
	t.Log(ws.SubscribeDepth(exchangeWs.BTC_USD, exchangeWs.NEXT_WEEK_CONTRACT, 0))
	t.Log(ws.SubscribeTrade(exchangeWs.LTC_USD, exchangeWs.THIS_WEEK_CONTRACT))
	time.Sleep(time.Minute)
}
