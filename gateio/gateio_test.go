package gateio

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var gate = New(http.DefaultClient, "", "")

func TestGate_GetTicker(t *testing.T) {
	ticker, err := gate.GetTicker(exchangeWs.BTC_USDT)
	t.Log("err=>", err)
	t.Log("ticker=>", ticker)
}

func TestGate_GetDepth(t *testing.T) {
	dep, err := gate.GetDepth(1, exchangeWs.BTC_USDT)

	t.Log("err=>", err)
	t.Log("asks=>", dep.AskList)
	t.Log("bids=>", dep.BidList)
}
