package okcoin

import (
	"github.com/stretchr/testify/assert"
	. "gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var (
	okex = NewOKEx(http.DefaultClient, "", "")
)

func TestOKEx_GetFutureDepth(t *testing.T) {
	dep, err := okex.GetFutureDepth(BTC_USD, QUARTER_CONTRACT, 1)
	assert.Nil(t, err)
	t.Log(dep)
}
