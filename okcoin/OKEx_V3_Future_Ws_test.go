package okcoin

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/XiaoyeFang/exchangeWs"
	"log"
	"sync"
	"testing"
)

func newOKExV3FutureWs() *OKExV3FutureWs {
	okV3Ws := NewOKExV3FutureWs(okexV3)
	okV3Ws.WsUrl("wss://okexcomreal.bafang.com:10442/ws/v3")
	okV3Ws.ErrorHandleFunc(func(err error) {
		log.Println(err)
	})
	return okV3Ws
}

var (
	okV3Ws = newOKExV3FutureWs()
)

func TestOKExV3FutureWsTickerCallback(t *testing.T) {
	n := 10
	tickers := make([]exchangeWs.FutureTicker, 0)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	okV3Ws.TickerCallback(func(ticker *exchangeWs.FutureTicker) {
		t.Log(ticker, ticker.Ticker)
		if len(tickers) <= n {
			tickers = append(tickers, *ticker)
		}
		if len(tickers) == n {
			wg.Done()
		}
	})
	okV3Ws.SubscribeTicker(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT)
	okV3Ws.SubscribeTicker(exchangeWs.EOS_USD, exchangeWs.SWAP_CONTRACT)
	wg.Wait()
}

func TestOKExV3FutureWsDepthCallback(t *testing.T) {
	n := 10
	depths := make([]exchangeWs.Depth, 0)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	okV3Ws.DepthCallback(func(depth *exchangeWs.Depth) {
		if len(depths) <= n {
			t.Log(depth)
			depths = append(depths, *depth)
		}
		if len(depths) == n {
			wg.Done()
		}
	})
	okV3Ws.SubscribeDepth(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT, 5)
	okV3Ws.SubscribeDepth(exchangeWs.EOS_USD, exchangeWs.SWAP_CONTRACT, 5)
	wg.Wait()
}

func TestOKExV3FutureWsTradeCallback(t *testing.T) {
	n := 10
	trades := make([]exchangeWs.Trade, 0)
	wg := &sync.WaitGroup{}
	wg.Add(1)
	okV3Ws.TradeCallback(func(trade *exchangeWs.Trade, contractType string) {
		if len(trades) <= n {
			t.Log(contractType, trade)
			trades = append(trades, *trade)
		}
		if len(trades) == n {
			wg.Done()
		}
	})
	okV3Ws.SubscribeTrade(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT)
	okV3Ws.SubscribeTrade(exchangeWs.EOS_USD, exchangeWs.SWAP_CONTRACT)
	wg.Wait()
}

func TestOKExV3FutureWsLogin(t *testing.T) {
	if authed {
		okV3Ws := newOKExV3FutureWs()
		err := okV3Ws.Login("", apiSecretKey, passphrase) // fail
		assert.True(t, err != nil)
		okV3Ws = newOKExV3FutureWs()
		err = okV3Ws.Login(apiKey, apiSecretKey, passphrase) //succes
		assert.Nil(t, err)
		err = okV3Ws.Login(apiKey, apiSecretKey, passphrase) //duplicate login
		assert.Nil(t, err)
	} else {
		t.Log("not authed, skip test websocket login")
	}
}

func placeAndCancel(currencyPair exchangeWs.CurrencyPair, contractType string) {
	leverage := 20
	depth := 100
	dep, _ := okexV3.GetFutureDepth(currencyPair, contractType, depth)
	price := fmt.Sprintf("%f", dep.BidList[depth-1].Price)
	symbol, _ := okexV3.GetContract(currencyPair, contractType)
	amount := symbol.getSizeIncrement()
	orderID, err := okexV3.PlaceFutureOrder(
		currencyPair, contractType, price, amount, exchangeWs.OPEN_BUY, 0, leverage)
	if err != nil {
		log.Println(err)
	}
	_, err = okexV3.FutureCancelOrder(currencyPair, contractType, orderID)
	if err != nil {
		log.Println(err)
	}
}

func TestOKExV3FutureWsOrderCallback(t *testing.T) {
	if authed {
		err := okV3Ws.Login(apiKey, apiSecretKey, passphrase)
		assert.Nil(t, err)
		n := 4
		orders := make([]exchangeWs.FutureOrder, 0)
		wg := &sync.WaitGroup{}
		wg.Add(1)
		okV3Ws.OrderCallback(func(order *exchangeWs.FutureOrder, contractType string) {
			if len(orders) <= n {
				t.Log(contractType, order)
				orders = append(orders, *order)
			}
			if len(orders) == n {
				wg.Done()
			}
		})
		err = okV3Ws.SubscribeOrder(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT)
		assert.Nil(t, err)
		err = okV3Ws.SubscribeOrder(exchangeWs.EOS_USD, exchangeWs.SWAP_CONTRACT)
		assert.Nil(t, err)
		placeAndCancel(exchangeWs.EOS_USD, exchangeWs.QUARTER_CONTRACT)
		placeAndCancel(exchangeWs.EOS_USD, exchangeWs.SWAP_CONTRACT)
		wg.Wait()
	}
}
