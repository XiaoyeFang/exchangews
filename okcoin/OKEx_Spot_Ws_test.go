package okcoin

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"testing"
	"time"
)

func TestNewOKExSpotWs(t *testing.T) {
	okSpotWs := NewOKExSpotWs()
	//	okSpotWs.ProxyUrl("socks5://127.0.0.1:1080")

	okSpotWs.SetCallbacks(func(ticker *exchangeWs.Ticker) {
		t.Log(ticker)
	}, func(depth *exchangeWs.Depth) {
		t.Log(depth)
	}, func(trade *exchangeWs.Trade) {
		t.Log(trade)
	}, func(kline *exchangeWs.Kline, i int) {
		t.Log(i, kline)
	})

	okSpotWs.ErrorHandleFunc(func(err error) {
		t.Log(err)
	})
	//	t.Log(okSpotWs.SubscribeTicker(exchangeWs.BTC_USDT))
	//	t.Log(okSpotWs.SubscribeTicker(exchangeWs.BCH_USDT))
	//okSpotWs.SubscribeDepth(exchangeWs.BTC_USDT, 5)
	//okSpotWs.SubscribeTrade(exchangeWs.BTC_USDT)
	t.Log(okSpotWs.SubscribeKline(exchangeWs.BTC_USDT, exchangeWs.KLINE_PERIOD_1H))
	time.Sleep(10 * time.Second)
}
