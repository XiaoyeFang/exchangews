package okcoin

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"testing"
	"time"
)

func TestNewOKExFutureWs(t *testing.T) {
	okWs := NewOKExFutureWs()
	okWs.ErrorHandleFunc(func(err error) {
		t.Log(err)
	})
	okWs.SetCallbacks(func(ticker *exchangeWs.FutureTicker) {
		t.Log(ticker, ticker.Ticker)
	}, func(depth *exchangeWs.Depth) {
		t.Log(depth.ContractType, depth.Pair, depth.AskList, depth.BidList)
	}, func(trade *exchangeWs.Trade, contract string) {
		t.Log(contract, trade)
	})
	okWs.SubscribeTicker(exchangeWs.LTC_USD, exchangeWs.QUARTER_CONTRACT)
	okWs.SubscribeDepth(exchangeWs.LTC_USD, exchangeWs.QUARTER_CONTRACT, 5)
	okWs.SubscribeTrade(exchangeWs.LTC_USD, exchangeWs.QUARTER_CONTRACT)
	time.Sleep(3 * time.Minute)
}
