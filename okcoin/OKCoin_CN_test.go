package okcoin

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var okcn = NewOKCoinCn(http.DefaultClient, "", "")

func TestOKCoinCN_API_GetTicker(t *testing.T) {
	ticker, _ := okcn.GetTicker(exchangeWs.BTC_CNY)
	t.Log(ticker)
}

func TestOKCoinCN_API_GetDepth(t *testing.T) {
	dep, _ := okcn.GetDepth(1, exchangeWs.ETH_CNY)
	t.Log(dep)
}

func TestOKCoinCN_API_GetKlineRecords(t *testing.T) {
	klines, _ := okcn.GetKlineRecords(exchangeWs.BTC_USDT, exchangeWs.KLINE_PERIOD_1MIN, 1000, -1)
	t.Log(klines)
}
