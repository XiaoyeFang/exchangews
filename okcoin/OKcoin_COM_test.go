package okcoin

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var okcom = NewCOM(http.DefaultClient, "", "")

func TestOKCoinCOM_API_GetTicker(t *testing.T) {
	ticker, _ := okcom.GetTicker(exchangeWs.BTC_CNY)
	t.Log(ticker)
}
