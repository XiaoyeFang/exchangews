package gdax

import (
	"gitlab.com/XiaoyeFang/exchangeWs"
	"net/http"
	"testing"
)

var gdax = New(http.DefaultClient, "", "")

func TestGdax_GetTicker(t *testing.T) {
	ticker, err := gdax.GetTicker(exchangeWs.BTC_USD)
	t.Log("err=>", err)
	t.Log("ticker=>", ticker)
}

func TestGdax_Get24HStats(t *testing.T) {
	stats, err := gdax.Get24HStats(exchangeWs.BTC_USD)
	t.Log("err=>", err)
	t.Log("stats=>", stats)
}

func TestGdax_GetDepth(t *testing.T) {
	dep, err := gdax.GetDepth(2, exchangeWs.BTC_USD)
	t.Log("err=>", err)
	t.Log("bids=>", dep.BidList)
	t.Log("asks=>", dep.AskList)
}
